﻿/*
 * upLib.h
 *
 * Created: 2018-07-05 오전 9:58:55
 *  Author: YongSeok
 */ 

#include "define.h"

#ifndef UPLIB_H_
#define UPLIB_H_

///////////////////////////////////////////////////////////
/////// user variable /////////////////////////////////////
///////////////////////////////////////////////////////////
// Uart buffers
extern U8 gUart0RxBuf[UART0_RX_BUF_SIZE];
extern S32 gUart0RxCnt;
extern bool gIsReceiveUart0;

extern U8 gUart1RxBuf[UART0_RX_BUF_SIZE];
extern S32 gUart1RxCnt;
extern bool gIsReceiveUart1;

///////////////////////////////////////////////////////////
/////// user function /////////////////////////////////////
///////////////////////////////////////////////////////////
//initialize
void InitializeMCU(void);
void initBufUART0();
void initBufUART1();
void reSetBT(void);

//DELAYs
void delay_ms(long _time_ms);
void delay_100us(long _time_us);

//LEDs
void upSideWhiteLED(int _side, int _onoff);
void upSideBlueLED(int _side, int _onoff);
void nearChartLED(int _onoff);
void blinkWhiteLED();
void blinkBlueLED();
void blinkWhiteBlueLED();
void allLEDsOFF();

//Uart
void UART0_TX_char(unsigned char data);
void UART0_TX_string(char * string);
void UART1_TX_char(unsigned char data);
void UART1_TX_string(char * string);




#endif /* UPLIB_H_ */