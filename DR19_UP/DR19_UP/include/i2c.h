﻿/*
 * i2c.h
 *
 * Created: 2018-04-30 오후 4:47:01
 *  Author: YongSeok
 */ 
   //asm volatile("__i2c_port=0x12");
    //.equ __i2c_port=0x12
    //.equ __sda_bit=1
    //.equ __scl_bit=0
    //#endasm
    // #include <i2c.h>


#ifndef I2C_H_
#define I2C_H_


#pragma used+
void i2c_init(void);
unsigned char i2c_start(void);
void i2c_stop(void);
unsigned char i2c_read(unsigned char ack);
unsigned char i2c_write(unsigned char data);
#pragma used-


#endif /* I2C_H_ */