﻿/*
 * upLib.c
 *
 * Created: 2018-04-30 오후 4:38:17
 *  Author: YongSeok
 */
 #include "include/define.h"
 #include "include/upLib.h"

//for uart
U8 gUart0RxBuf[UART0_RX_BUF_SIZE]={0,};
S32 gUart0RxCnt = 0;
U8 gUart1RxBuf[UART0_RX_BUF_SIZE]={0,};
S32 gUart1RxCnt = 0;
bool gIsReceiveUart0 = false;
bool gIsReceiveUart1 = false;

////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////  MCU INIT  ////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////
void InitializeMCU(void)
{
	
	cli();
    /*
    * PORTX  : Data Register
    * DDRX   : Data Direction Register
    - 0: Input
    - 1: Output                     */
    /*----------------------------------------------
    PORT A- Motor3 & NEAR_LED(PA6) & FRONT_LED(PA7)
    PA0 - STEP
    PA1 - DIR
    PA2 - MS1
    PA3 - MS2
    PA4 - ENABLE - high is disable
    PA5 - SLEEP  - low is sleep
    PA6 - NEAR_LED
    PA7 - LEFT LED(BLUE)


		 7  6  5  4.  3  2  1  0
	DDR  1  1  1  1.  1  1  1  1
    DATA 0  0  0  1.  0  1  0  0
    ---------------------------------------------- */
    DDRA =0xFF;
    PORTA=0x14;


    /*----------------------------------------------
    PORT B-         Motor1
    PB0 - BT_RESET
    PB1 - SCK  - Download Pin
    PB2 - STEP
    PB3 - DIR
    PB4 - MS1
    PB5 - MS2
    PB6 - ENABLE - high is disable
    PB7 - SLEEP  - low is sleep

		 7  6  5  4  3  2  1  0
    DDR  1  1  1  1. 1  1  0  1          output
    DATA 0  1  0  1. 0  0  0  1
    ---------------------------------------------- */
    DDRB =0xFD;
    PORTB=0x51;
	
	
	 
    /*----------------------------------------------
    PORT C- Motor2
    PC0 - STEP
    PC1 - DIR
    PC2 - MS1
    PC3 - MS2
    PC4 - ENABLE - high is disable
    PC5 - SLEEP  - low is sleep
    PC6 -
    PC7 -


		 7  6  5  4  3  2  1  0
    DDR  0  0  1  1. 1  1  1  1
    DATA 0  0  0  1. 0  1  0  0
    ---------------------------------------------- */
    DDRC =0x3F;
    PORTC=0x14; //
	
	//DDR 0 0 0 0. 1 0 0 0
	//    0 0 0 0. 0 0 0 0
    PORTD&=~(0x1<<4);
    DDRD  =DDRD |  (0x1<<4);


    /*----------------------------------------------
    PORT E
    PE0 - RX RS485 for BODY LOW
    PE1 - TX RS485 for BODY LOW
    PE2 -  485_CONT  :   485  TX일때 Low, Rx일 때  High
    PE3 - NEAR SENSOR
    PE4 - L_PD_SENSOR
    PE5 - R_PD_SENSOR
    PE6 - L_TILT_SENSOR
    PE7 - R_TILT_SENSOR


		 7  6  5  4  3  2  1  0
    DDR  0  0  0  0. 0  1  1  0
    DATA 0  0  0  0. 0  0  0  0
    ---------------------------------------------- */
    DDRE=0x06;
    PORTE=0x00;


    /*----------------------------------------------
    PORT F- Motor4
    PF0 - STEP
    PF1 - DIR
    PF2 - MS1
    PF3 - MS2
    PF4 - ENABLE - high is disable
    PF5 - SLEEP  - low is sleep
    PF6 - LEFT  LED high is ON
    PF7 - RIGHT LED

		 7  6  5  4  3  2  1  0
    DDR  1  1  1  1. 1  1  1  1
    DATA 0  0  0  1. 0  1  0  0
    ---------------------------------------------- */
    DDRF =0xFF;
	PORTF=0x14;

    /*----------------------------------------------
    PORT G- ForHead_Detect

    PG3 - ForHead_Detect sensor
    PG4 - RIGHT LED(BLUE)
		 7  6  5  4  3  2  1  0
    DDR  0  0  0  1. 0  0  0  0
    DATA 0  0  0  0. 0  0  0  0
    ---------------------------------------------- */
    DDRG=0x10;
    PORTG=0x00;
	
	

    // USART0 initialization for 485 Body Low
    // Communication Parameters: 8 Data, 1 Stop, No Parity
    // USART0 Receiver: On
    // USART0 Transmitter: On
    // USART0 Mode: Asynchronous
    // USART0 Baud rate: 9600
    /*UCSR0A=0x00;
    UCSR0B=0x18; //RxInttrupt On-> 0x98
    UCSR0C=0x06;
    UBRR0H=0x00;
    UBRR0L=0x67;    */
    UCSR0A=0x00;
    UCSR0B=0x98;
    UCSR0C=0x06;
    UBRR0H=0x00;
    UBRR0L=0x67; // 9600: 103, 112500: 8

    // USART1 initialization
    // Communication Parameters: 8 Data, 1 Stop, No Parity
    // USART1 Receiver: On
    // USART1 Transmitter: On
    // USART1 Mode: Asynchronous
    // USART1 Baud rate: 9600
    UCSR1A=0x00;
    UCSR1B=0x98;
    UCSR1C=0x06;
    UBRR1H=0x00;
    UBRR1L=0x67; // 9600: 103, 112500: 8

    // Timer/Counter 0 initialization
    // Clock source: System Clock
    // Clock value: 250.000 kHz
    // Mode: Normal top=0xFF
    // OC0 output: Disconnected
    ASSR=0x00;
    TCCR0=0x04;
    TCNT0=0x05;
    OCR0=0x00;

    // Timer/Counter 1 initialization
    // Clock source: System Clock
    // Clock value: Timer1 Stopped
    // Mode: Normal top=0xFFFF
    // OC1A output: Discon.
    // OC1B output: Discon.
    // OC1C output: Discon.
    // Noise Canceler: Off
    // Input Capture on Falling Edge
    // Timer1 Overflow Interrupt: Off
    // Input Capture Interrupt: Off
    // Compare A Match Interrupt: Off
    // Compare B Match Interrupt: Off
    // Compare C Match Interrupt: Off
    TCCR1A=0x00;
    TCCR1B=0x00;
    TCNT1H=0x00;
    TCNT1L=0x00;
    ICR1H=0x00;
    ICR1L=0x00;
    OCR1AH=0x00;
    OCR1AL=0x00;
    OCR1BH=0x00;
    OCR1BL=0x00;
    OCR1CH=0x00;
    OCR1CL=0x00;

    // Timer/Counter 2 initialization
    // Clock source: System Clock
    // Clock value: Timer2 Stopped
    // Mode: Normal top=0xFF
    // OC2 output: Disconnected
    TCCR2=0x00;
    TCNT2=0x00;
    OCR2=0x00;

    // Timer/Counter 3 initialization
    // Clock source: System Clock
    // Clock value: Timer3 Stopped
    // Mode: Normal top=0xFFFF
    // OC3A output: Discon.
    // OC3B output: Discon.
    // OC3C output: Discon.
    // Noise Canceler: Off
    // Input Capture on Falling Edge
    // Timer3 Overflow Interrupt: Off
    // Input Capture Interrupt: Off
    // Compare A Match Interrupt: Off
    // Compare B Match Interrupt: Off
    // Compare C Match Interrupt: Off
    TCCR3A=0x00;
    TCCR3B=0x00;
    TCNT3H=0x00;
    TCNT3L=0x00;
    ICR3H=0x00;
    ICR3L=0x00;
    OCR3AH=0x00;
    OCR3AL=0x00;
    OCR3BH=0x00;
    OCR3BL=0x00;
    OCR3CH=0x00;
    OCR3CL=0x00;
    // Timer(s)/Counter(s) Interrupt(s) initialization
    TIMSK=0x01;

    // Analog Comparator initialization
    // Analog Comparator: Off
    // Analog Comparator Input Capture by Timer/Counter 1: Off
    ACSR=0x80;
    SFIOR=0x00;

    // ADC initialization
    // ADC disabled
    ADCSRA=0x00;
    // SPI initialization
    // SPI disabled
    SPCR=0x00;

    // TWI initialization
    // TWI disabled
    TWCR=0x00;
	sei();
	//cli();
}


////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////// INIT  ////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////
void reSetBT(void)
{
	// PB0 - BT_RESET
	PORTB &=~(0x01); // PB0 is Low
	delay_ms(10); //10ms
	PORTB |=(0x01);  //  PB0 is High
	blinkWhiteBlueLED();
	allLEDsOFF();
}

void initBufUART0()
{
	gUart0RxCnt = 0;//low
	gIsReceiveUart0 = false;//low
	for(int i = 0 ; i < UART0_RX_BUF_SIZE ; i++)//low
	{
		gUart0RxBuf[i] = 0;
	}
}

void initBufUART1()
{
	gUart1RxCnt = 0;//bt
	gIsReceiveUart1 = false;//bt
	for(int i = 0 ; i < UART1_RX_BUF_SIZE ; i++)//bt
	{
		gUart1RxBuf[i] = 0;
	}
}


////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////  LED  /////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////
void upSideWhiteLED(int _side, int _onoff)
{
	switch(_side)
	{
		case LEFT:
			if(_onoff)	
			{
				PORTF |= (0x01 << PF6);
			}
			else        
			{
				PORTF &= ~(0x01 << PF6);
			}
		break;
		case RIGHT:
			if(_onoff)
			{
				PORTF |= (0x01 << PF7);
			}
			else
			{
				PORTF &= ~(0x01 << PF7);
			}
		break;
		case BOTH:
			if(_onoff)
			{
				PORTF |= (0x01 << PF6);
				PORTF |= (0x01 << PF7);
			}
			else
			{
				PORTF &= ~(0x01 << PF6);
				PORTF &= ~(0x01 << PF7);
			}
		break;
	}
}

void upSideBlueLED(int _side, int _onoff)
{
	switch(_side)
	{
		case LEFT:
			if(_onoff)
			{
				PORTA |= (0x01 << PA7);
			}
			else
			{
				PORTA &= ~(0x01 << PA7);
			}
		break;
		case RIGHT:
			if(_onoff)
			{
				PORTG |= (0x01 << PG4);
			}
			else
			{
				PORTG &= ~(0x01 << PG4);
			}
		break;
		case BOTH:
			if(_onoff)
			{
				PORTA |= (0x01 << PA7);
				PORTG |= (0x01 << PG4);
			}
			else
			{
				PORTA &= ~(0x01 << PA7);
				PORTG &= ~(0x01 << PG4);
			}
		break;
	}
}

void nearChartLED(int _onoff)
{
	if(_onoff)
	{
		PORTA |= (0x01 << PA6);
	}
	else
	{
		PORTA &= ~(0x01 << PA6);
	}
}

void blinkWhiteLED()
{
	PORTF |= (0x01 << PF6);
	PORTF |= (0x01 << PF7);
	delay_ms(20);
	PORTF &= ~(0x01 << PF6);
	PORTF &= ~(0x01 << PF7);
	delay_ms(20);
}
void blinkBlueLED()
{
	PORTA |= (0x01 << PA7);
	PORTG |= (0x01 << PG4);
	delay_ms(20);
	PORTA &= ~(0x01 << PA7);
	PORTG &= ~(0x01 << PG4);
	delay_ms(20);
}

void blinkWhiteBlueLED()
{
	PORTF |= (0x01 << PF6);
	PORTA &= ~(0x01 << PA7);
	delay_ms(10);
	PORTF |= (0x01 << PF7);
	PORTG &= ~(0x01 << PG4);
	delay_ms(10);
	PORTF &= ~(0x01 << PF6);
	PORTA |= (0x01 << PA7);
	delay_ms(10);
	PORTF &= ~(0x01 << PF7);
	PORTG |= (0x01 << PG4);
	delay_ms(10);
}
void allLEDsOFF()
{
	PORTF &= ~(0x01 << PF6);
	PORTF &= ~(0x01 << PF7);
	PORTA &= ~(0x01 << PA7);
	PORTG &= ~(0x01 << PG4);
}

//////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////  DELAY  /////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////
void delay_ms(long _time_ms)
{
	
	long i;
	for(i = 0 ; i < _time_ms ; i++)
	{
		_delay_us(250);
		_delay_us(250);
		_delay_us(250);
		_delay_us(250);
	}
}

void delay_100us(long _time_us)
{
	
	long i;
	for(i = 0 ; i < _time_us ; i++)
	{
		_delay_us(100);
	}
}


//////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////  TIMER  /////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////
int tesetCnt = 0;
ISR(TIMER0_OVF_vect)
{
	// Reinitialize Timer 0 value
	
	TCNT0=0x05;
	tesetCnt++;
	if(tesetCnt > 5000)	
	{
		UART1_TX_char('A');
		tesetCnt = 0;
	}
	
	
}
//////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////  UART0  /////////////////////////////////////////
////////////////////////////////////////LOW BOARD/////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////
ISR(USART0_RX_vect)
{
	gUart0RxBuf[gUart0RxCnt] = UDR0;
	gIsReceiveUart0 = true;
	gUart0RxCnt++;
	if(gUart0RxCnt > UART0_RX_BUF_SIZE)	
	{
		gUart0RxCnt = 0;
		gIsReceiveUart0 = false;
	}
}
void UART0_TX_char(unsigned char data)
{
	while((UCSR0A & 0x20)==0);
	UDR0 = data;
}

void UART0_TX_string(char * string)
{
	while(*string != '\0')
	{
		UART0_TX_char(*string);
		string++;
	}
}

//////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////  UART1  /////////////////////////////////////////
//////////////////////////////////////  BLUETOOTH ////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////
bool testbool = false;
ISR(USART1_RX_vect)
{
	gIsReceiveUart1 = true;
	gUart1RxBuf[gUart1RxCnt] = UDR1;
	gUart1RxCnt++;
	if(gUart1RxCnt > UART1_RX_BUF_SIZE)
	{
		gUart1RxCnt = 0;
		gIsReceiveUart1 = false;
	}
	
}
void UART1_TX_char(unsigned char data)
{
	while((UCSR1A & 0x20)==0);
	UDR1 = data;
}

void UART1_TX_string(char * string)
{
	while(*string != '\0')
	{
		UART1_TX_char(*string);
		string++;
	}
}

//////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////  MOTOR  /////////////////////////////////////////
/////////////////////////////////////////  TILT //////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////
void setTiltMotorMode(U8 _mode)
{
	switch(_mode)
	{
		case FULL:// MS1(LOW)  MS2(LOW)
		PORTC &= ~(0x01 << PC2);//MS1
		PORTC &= ~(0x01 << PC3);//MS2
		PORTF &= ~(0x01 << PF2);//MS1
		PORTF &= ~(0x01 << PF3);//MS2
		break;
		case HALF:// MS1(HIGH)  MS2(LOW)
		PORTC |= (0x01 << PC2);//MS1
		PORTC &= ~(0x01 << PC3);//MS2
		PORTF |= (0x01 << PF2);//MS1
		PORTF &= ~(0x01 << PF3);//MS2
		break;
		case QUARTER:// MS1(LOW)  MS2(HIGH)
		PORTC &= ~(0x01 << PC2);//MS1
		PORTC |= (0x01 << PC3);//MS2
		PORTF &= ~(0x01 << PF2);//MS1
		PORTF |= (0x01 << PF3);//MS2
		break;
		case EIGHTH:// MS1(HIGH)  MS2(HIGH)
		PORTC |= (0x01 << PC2);//MS1
		PORTC |= (0x01 << PC3);//MS2
		PORTF |= (0x01 << PF2);//MS1
		PORTF |= (0x01 << PF3);//MS2
		break;
	}
}

//prepare for move
void tiltMotorEnable(int _dir)
{
	setTiltMotorMode(TILT_MOTOR_MODE);
	switch(_dir)
	{
		case LEFT:
		LEFT_TILT_STEP_LOW();
		LEFT_TILT_ENABLE();
		//RIGHT_TILT_WAKEUP();
		//LEFT_TILT_WAKEUP();
		break;
		case RIGHT:
		RIGHT_TILT_STEP_LOW();
		RIGHT_TILT_ENABLE();
		//RIGHT_TILT_WAKEUP();
		//LEFT_TILT_WAKEUP();
		break;
		case BOTH:
		RIGHT_TILT_STEP_LOW();
		LEFT_TILT_STEP_LOW();
		RIGHT_TILT_ENABLE();
		LEFT_TILT_ENABLE();
		//RIGHT_TILT_WAKEUP();
		//LEFT_TILT_WAKEUP();
		break;
	}
	// ENABLE/DISABLE: 모터쪽으로의 전류만 차단.
	// WAKEUP/SLEEP  : 모터쪽으로의 전류 차단 및 절전, 종료(재부팅)
}

void tiltMotorDisable(int _dir)
{
	switch(_dir)
	{
		case LEFT:
		LEFT_TILT_DISABLE();
		//RIGHT_TILT_WAKEUP();
		//LEFT_TILT_WAKEUP();
		break;
		case RIGHT:
		RIGHT_TILT_DISABLE();
		//RIGHT_TILT_WAKEUP();
		//LEFT_TILT_WAKEUP();
		break;
		case BOTH:
		RIGHT_TILT_DISABLE();
		LEFT_TILT_DISABLE();
		//RIGHT_TILT_WAKEUP();
		//LEFT_TILT_WAKEUP();
		break;
	}
	// ENABLE/DISABLE: 모터쪽으로의 전류만 차단.
	// WAKEUP/SLEEP  : 모터쪽으로의 전류 차단 및 절전, 종료(재부팅)
}

//motor 방향성 설정
void setTiltDirection(int _side, int _dir)
{
	if(_dir == CW)
	{
		if(_side == LEFT)			PORTF |= (0x01 << PF1);
		else if(_side == RIGHT)		PORTC |= (0x01 << PC1);
	}
	else if(_dir == CCW)
	{
		if(_side == LEFT)			PORTF &= ~(0x01 << PF1);
		else if(_side == RIGHT)		PORTC &= ~(0x01 << PC1);
	}
}

//left tilt motor move
void moveTiltMotorLeft()
{
	LEFT_TILT_STEP_HIGH();
	delay_100us(TILT_MOTOR_SPEED);
	LEFT_TILT_STEP_LOW();
	delay_100us(TILT_MOTOR_SPEED);
	LEFT_TILT_STEP_HIGH();
	delay_100us(TILT_MOTOR_SPEED);
	LEFT_TILT_STEP_LOW();
	delay_100us(TILT_MOTOR_SPEED);
}

//right tilt motor move
void moveTiltMotorRight()
{
	RIGHT_TILT_STEP_HIGH();
	delay_100us(TILT_MOTOR_SPEED);
	RIGHT_TILT_STEP_LOW();
	delay_100us(TILT_MOTOR_SPEED);
	RIGHT_TILT_STEP_HIGH();
	delay_100us(TILT_MOTOR_SPEED);
	RIGHT_TILT_STEP_LOW();
	delay_100us(TILT_MOTOR_SPEED);
}

//both tilt motor move
void moveTiltMotorBoth()
{
	RIGHT_TILT_STEP_HIGH();
	LEFT_TILT_STEP_HIGH();
	delay_100us(TILT_MOTOR_SPEED);
	RIGHT_TILT_STEP_LOW();
	LEFT_TILT_STEP_LOW();
	delay_100us(TILT_MOTOR_SPEED);
	RIGHT_TILT_STEP_HIGH();
	LEFT_TILT_STEP_HIGH();
	delay_100us(TILT_MOTOR_SPEED);
	RIGHT_TILT_STEP_LOW();
	LEFT_TILT_STEP_LOW();
	delay_100us(TILT_MOTOR_SPEED);
}

//initialize tilt
void initMotorTilt()
{
	int i = 0;
	int findSensor = FIND_SENSOR_NONE;
	int leftTiltInitPos = 300;
	int rightTiltInitPos = 300;
	
	//prepare tilt motor for move
	LEFT_TILT_WAKEUP();
	RIGHT_TILT_WAKEUP();
	tiltMotorEnable(BOTH);
	//초기화시에 센서가 인식되어 있다면  자석을 벗어나도록
	if(READ_LEFT_TILT_SENSOR || READ_RIGHT_TILT_SENSOR)
	{
		setTiltDirection(RIGHT, CW);
		setTiltDirection(LEFT, CCW);
		for(i = 0 ; i < 10 ; i++)
		{
			moveTiltMotorBoth();
		}
		delay_100us(10);		
	}
	tiltMotorDisable(BOTH);
	
	//tilt 방향성 설정
	setTiltDirection(RIGHT, CW);
	setTiltDirection(LEFT, CCW);
	
	//prepare tilt motor for move
	tiltMotorEnable(BOTH);
	//센서 찾기위해 move
	for(i = 0 ; i < TILT_MOTOR_MAX ; i++)
	{
		if(READ_LEFT_TILT_SENSOR && READ_RIGHT_TILT_SENSOR)//양쪽 모두 센싱
		{
			findSensor = FIND_SENSOR_BOTH;
		}
		else if(READ_LEFT_TILT_SENSOR || READ_RIGHT_TILT_SENSOR)
		{
			if(READ_RIGHT_TILT_SENSOR)//오른쪽만 센싱
			{
				findSensor = FIND_SENSOR_RIGHT;
				moveTiltMotorLeft();
			}
			else if(READ_LEFT_TILT_SENSOR)//왼쪽만 센싱
			{
				findSensor = FIND_SENSOR_LEFT;
				moveTiltMotorRight();
			}
		}
		else//센싱 안되었을때
		{
			moveTiltMotorBoth();
		}
	}
	
	
	//초기위치로 move
	if(findSensor == FIND_SENSOR_BOTH)//양쪽 다 찾았을 경우
	{
		setTiltDirection(RIGHT, CW);
	}
	else if(findSensor == FIND_SENSOR_BOTH)//왼쪽만 찾았을 경우
	{
		
	}
	else if(findSensor == FIND_SENSOR_BOTH)//오른쪽만 찾았을 경우
	{
		
	}
	else if(findSensor == FIND_SENSOR_NONE)//모두 못 찾았을 경우 
	{
		
	}
	
}