﻿/*
 * define.h
 *
 * Created: 2018-04-30 오후 4:37:43
 *  Author: YongSeok
 */ 


#include <stdio.h>
#include <stdlib.h>
#include <util/twi.h>
#include <avr/interrupt.h>
#include <util/atomic.h>
#include <stdbool.h>
#include <util/delay.h>
//#include "upLib.h"

#ifndef DEFINE_H_
#define DEFINE_H_


typedef unsigned char	U8;			/* data type definition */
typedef   signed char	S8;
typedef unsigned int	U16;
typedef   signed int	S16;
typedef unsigned long	U32;
typedef   signed long	S32;

#define BV(bit)		(1<<(bit))		/* bit processing */
#define cbi(reg,bit)	reg &= ~(BV(bit))
#define sbi(reg,bit)	reg |= (BV(bit))


/* Port A Data Register - PORTA */
#define    PA7       7
#define    PA6       6
#define    PA5       5
#define    PA4       4
#define    PA3       3
#define    PA2       2
#define    PA1       1
#define    PA0       0

/* Port B Data Register - PORTB */
#define    PB7       7
#define    PB6       6
#define    PB5       5
#define    PB4       4
#define    PB3       3
#define    PB2       2
#define    PB1       1
#define    PB0       0

/* Port C Data Register - PORTC */
#define    PC7       7
#define    PC6       6
#define    PC5       5
#define    PC4       4
#define    PC3       3
#define    PC2       2
#define    PC1       1
#define    PC0       0


/* Port D Data Register - PORTD */
#define    PD7       7
#define    PD6       6
#define    PD5       5
#define    PD4       4
#define    PD3       3
#define    PD2       2
#define    PD1       1
#define    PD0       0

/* Port E Data Register - PORTE */
#define    PE7       7
#define    PE6       6
#define    PE5       5
#define    PE4       4
#define    PE3       3
#define    PE2       2
#define    PE1       1
#define    PE0       0

/* Port F Data Register - PORTF */
#define    PF7       7
#define    PF6       6
#define    PF5       5
#define    PF4       4
#define    PF3       3
#define    PF2       2
#define    PF1       1
#define    PF0       0

/* Port G Data Register - PORTG */
#define    PG7       7
#define    PG6       6
#define    PG5       5
#define    PG4       4
#define    PG3       3
#define    PG2       2
#define    PG1       1
#define    PG0       0

////////////////////////////////////////////////////////////////
// user define /////////////////////////////////////////////////
////////////////////////////////////////////////////////////////
// i/o
#define LEFT					(0)
#define RIGHT					(1)
#define BOTH					(2)
#define ON						(1)
#define OFF						(0)
#define CW						(1)
#define CCW						(-1)

#define FIND_SENSOR_NONE		(0)
#define FIND_SENSOR_RIGHT		(1)
#define FIND_SENSOR_LEFT		(2)
#define FIND_SENSOR_BOTH		(3)

//uart
#define UART0_RX_BUF_SIZE		(100)
#define UART1_RX_BUF_SIZE		(50)

//mode
#define FULL	(0)
#define HALF	(1)
#define QUARTER (2)
#define EIGHTH  (3)

//motor properties
#define TILT_MOTOR_MODE		QUARTER
#define TILT_MOTOR_SPEED	(10)
#define TILT_MOTOR_MAX		(600)
#define PD_MOTOR_MODE		QUARTER
#define PD_MOTOR_SPEED		(10)
#define PD_MOTOR_MAX		(600)

//sensor - tilt
#define READ_LEFT_TILT_SENSOR			(PINE &(1 << PE6))//(PINE.6)
#define READ_RIGHT_TILT_SENSOR			(PINE &(1 << PE7))//(PINE.7)

//sensor - pd
#define READ_LEFT_PD_SENSOR				(PINE &(1 << PE5))//(PINE.5)
#define READ_RIGHT_PD_SENSOR			(PINE &(1 << PE4))//(PINE.4)

//motor - tilt
#define RIGHT_TILT_STEP_LOW()				(PORTC &= ~(0x01<<PC0))
#define RIGHT_TILT_STEP_HIGH()				(PORTC |=  (0x01<<PC0))
#define RIGHT_TILT_ENABLE()					(PORTC &= ~(0x01<<PC4))//LOW is enable
#define RIGHT_TILT_DISABLE()				(PORTC |=  (0x01<<PC4))//HIGH is disable
#define RIGHT_TILT_SLEEP()					(PORTC &= ~(0x01<<PC5))
#define RIGHT_TILT_WAKEUP()					(PORTC |=  (0x01<<PC5))

#define LEFT_TILT_STEP_LOW()				(PORTF &= ~(0x01<<PF0))
#define LEFT_TILT_STEP_HIGH()				(PORTF |=  (0x01<<PF0))
#define LEFT_TILT_ENABLE()					(PORTF &= ~(0x01<<PF4))//LOW is enable
#define LEFT_TILT_DISABLE()					(PORTF |=  (0x01<<PF4))//HIGH is disable
#define LEFT_TILT_SLEEP()					(PORTF &= ~(0x01<<PF5))
#define LEFT_TILT_WAKEUP()					(PORTF |=  (0x01<<PF5))


//motor - pd
#define RIGHT_PD_STEP_LOW()					(PORTB &= ~(0x01<<PB2))
#define RIGHT_PD_STEP_HIGH()				(PORTB |=  (0x01<<PB2))
#define RIGHT_PD_ENABLE()					(PORTB &= ~(0x01<<PB6))//LOW is enable
#define RIGHT_PD_DISABLE()					(PORTB |=  (0x01<<PB6))//HIGH is disable
#define RIGHT_PD_SLEEP()					(PORTB &= ~(0x01<<PB7))
#define RIGHT_PD_WAKEUP()					(PORTB |=  (0x01<<PB7))

#define LEFT_PD_STEP_LOW()					(PORTA &= ~(0x01<<PA0))
#define LEFT_PD_STEP_HIGH()					(PORTA |=  (0x01<<PA0))
#define LEFT_PD_ENABLE()					(PORTA &= ~(0x01<<PA4))//LOW is enable
#define LEFT_PD_DISABLE()					(PORTA |=  (0x01<<PA4))//HIGH is disable
#define LEFT_PD_SLEEP()						(PORTA &= ~(0x01<<PA2))
#define LEFT_PD_WAKEUP()					(PORTA |=  (0x01<<PA5))



#endif /* DEFINE_H_ */

















